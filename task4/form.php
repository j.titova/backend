<html lang="ru">
<head>
    <meta charset="UTF-8">
    <title>TASK 4</title>
    <style>
       form {
           width: 600px;
           height: 700px;
           background: white;
           border-radius: 8px;
           margin: 0 auto;
           padding: 30px;
           box-shadow: 0px 0px 14px 0px rgba(50, 50, 50, 0.75);
       }
       p {
           line-height: 0.5;
       }
       label {
           margin: 3px;
       }
       input {
           margin: 8px 0;
       }
       input[type="text"], input[type="email"] {
           width: 100%;
           height: 30px;
           border-radius: 5px;
           border: 2px solid grey;
           outliine: none;
           padding: 7px;
       }
       input[type="checkbox"] {
           margin-right: 7px;
       }
       textarea {
           width: 300px;
           height: 150px;
           padding: 7px;
       }
       input[type="submit"] {
           padding: 7px 20px;
           border-radius: 5px;
           box-shadow: 0px 0px 5px 0px rgba(46, 53, 55, 0.5);
       }
       input[type="submit"]:hover {
           cursor: pointer;
       }
           .error {
               border: 1px red solid !important;
           }
    </style>
</head>
<body>
<?php
if (!empty($messages)) {
  print('<div id="messages">');
  // Выводим все сообщения.
  foreach ($messages as $message) {
    print($message);
  }
  print('</div>');
}

// Далее выводим форму отмечая элементы с ошибками классом error
// и задавая начальные значения элементов ранее сохраненными.
?>

<form action="" method="POST">
    <label>
      Имя:<br />
      <input name="fio" <?php if ($errors['fio']) print('class="error"');?>  value="<?php print($values[fio]);?>"  />
    </label><br />
    <label>
      Email:<br />
      <input name="email" <?php if ($errors['email']) print('class="error"');?>
        value="<?php print($values[email]);?>"
        type="email" />
    </label><br />
    <label>
      Год рождения:<br />
    <select name="year" >
      <?php for ($i = 1900; $i < 2021;$i++) {
        print('<option value="');
        print($i);
        if ($values['year'] == $i) print('" selected="selected">');
    else print('">');
        print($i);
        print('</option>');}?>
    </select>
    </label><br />
    Пол:<br />
    <label><input type="radio"
      name="sex" <?php if ($values['sex']==0){print 'checked';} ?> value="0" />
      М</label>
    <label><input type="radio" <?php if ($values['sex']==1){print 'checked';} ?>
      name="sex" value="1" />
      Ж</label><br />
    Количество конечностей:<br />
    <label><input type="radio" <?php if ($values['limb']==0 || $values['limb']==1){print 'checked';} ?>
      name="limb" value="1" />
      1</label>
    <label><input type="radio" <?php if ($values['limb']==2){print 'checked';} ?>
      name="limb" value="2" />
      2</label>
      <label><input type="radio" <?php if ($values['limb']==3){print 'checked';} ?>
      name="limb" value="3" />
      3</label>
      <label><input type="radio" <?php if ($values['limb']==4){print 'checked';} ?>
      name="limb" value="4" />
      4</label>   
	 <label>

<br />
    <label>
        Способности:
        <br />
        <select name="power[]" multiple="multiple">
        <option <?php if (in_array("god",$values['power'])){print 'selected="selected"';} ?> value="god">Бессмертие</option>
        <option <?php if (in_array("clip",$values['power'])){print 'selected="selected"';} ?> value="clip">Прохождение сквозь стены</option>
        <option <?php if (in_array("fly",$values['power'])){print 'selected="selected"';} ?> value="fly">Левитация</option>
        </select>
    </label><br />
    <label>
      Биография:<br />
      <textarea name="bio"><?php print($values['bio']);?> </textarea>
    </label><br />
    <label><input type="checkbox"
      name="check" required/>
      С контрактом ознакомлен</label><br />
    <input type="submit" value="Отправить" />
</form>
</body>
</html>
