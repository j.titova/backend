<?php
/**
* Реализовать проверку заполнения обязательных полей формы в предыдущей
* с использованием Cookies, а также заполнение формы по умолчанию ранее
* введенными значениями.
*/

// Отправляем браузеру правильную кодировку,
// файл index.php должен быть в кодировке UTF-8 без BOM.
header('Content-Type: text/html; charset=UTF-8');

// В суперглобальном массиве $_SERVER PHP сохраняет некторые заголовки запроса HTTP
// и другие сведения о клиненте и сервере, например метод текущего запроса $_SERVER['REQUEST_METHOD'].
if ($_SERVER['REQUEST_METHOD'] == 'GET') {
// Массив для временного хранения сообщений пользователю.
    $messages = array();

    if (!empty($_COOKIE['save'])) {
        setcookie('save', '', 100000);
        $messages[] = 'Спасибо, результаты сохранены.';
    }

    // Складываем признак ошибок в массив.
    $errors = array();
    $errors['fio'] = !empty($_COOKIE['fio_error']);
    $errors['email'] = !empty($_COOKIE['email_error']);
    $errors['power'] = !empty($_COOKIE['power_error']);

    // TODO: аналогично все поля.

    // Выдаем сообщения об ошибках.
    if ($errors['fio']) {
        // Удаляем куку, указывая время устаревания в прошлом.
        setcookie('fio_error', '', 100000);
        // Выводим сообщение.
        $messages[] = '<div class="error">Заполните имя.</div>';
    }
    if ($errors['email'] == 1) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Заполните email.</div>';
    } else if ($errors['email'] == 2) {
        setcookie('email_error', '', 100000);
        $messages[] = '<div class="error">Некорректный email.</div>';
    }
    if ($errors['power']) {
        setcookie('power_error', '', 100000);
        $messages[] = '<div class="error">Выберете суперспособность.</div>';
    }
    
    // TODO: тут выдать сообщения об ошибках в других полях.

    // Складываем предыдущие значения полей в массив, если есть.
    $values = array();
    $values['fio'] = empty($_COOKIE['fio_value']) ? '' : $_COOKIE['fio_value'];
    $values['email'] = empty($_COOKIE['email_value']) ? '' : $_COOKIE['email_value'];
    $values['year'] = empty($_COOKIE['year_value']) ? '' : $_COOKIE['year_value'];
    $values['sex'] = empty($_COOKIE['sex_value']) ? '' : $_COOKIE['sex_value'];
    $values['limb'] = empty($_COOKIE['limb_value']) ? '' : $_COOKIE['limb_value'];
    $values['power'] = empty($_COOKIE['power_value']) ? '' : unserialize($_COOKIE['power_value']);
    $values['bio'] = empty($_COOKIE['bio_value']) ? '' : $_COOKIE['bio_value'];


    // TODO: аналогично все поля.

    // Включаем содержимое файла form.php.
    // В нем будут доступны переменные $messages, $errors и $values для вывода
    // сообщений, полей с ранее заполненными данными и признаками ошибок.
    include('form.php');
}
// Иначе, если запрос был методом POST, т.е. нужно проверить данные и сохранить их в XML-файл.
else {
    // Проверяем ошибки.
    $errors = FALSE;
    if (empty($_POST['fio'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('fio_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('fio_value', $_POST['fio'], time() + 30 * 24 * 60 * 60);
    }

    // *************
    // TODO: тут необходимо проверить правильность заполнения всех остальных полей.
    // Сохранить в Cookie признаки ошибок и значения полей.
    // *************
    if (empty($_POST['email'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('email_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else if (!filter_var($_POST['email'], FILTER_VALIDATE_EMAIL)){
         // Выдаем куку на день с флажком об ошибке в поле fio.
         setcookie('email_error', '2', time() + 24 * 60 * 60);
         $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('email_value', $_POST['email'], time() + 30 * 24 * 60 * 60);
    }
             
    if (empty($_POST['power'])) {
        // Выдаем куку на день с флажком об ошибке в поле fio.
        setcookie('power_error', '1', time() + 24 * 60 * 60);
        $errors = TRUE;
    }
    else {
        // Сохраняем ранее введенное в форму значение на месяц.
        setcookie('power_value', serialize($_POST['power']), time() + 30 * 24 * 60 * 60);
    }
    
    if ($errors == false) {
        setcookie('year_value', $_POST['year'], time() + 30 * 24 * 60 * 60);
        setcookie('sex_value', $_POST['sex'], time() + 30 * 24 * 60 * 60);
        setcookie('limb_value', $_POST['limb'], time() + 30 * 24 * 60 * 60);
        setcookie('bio_value', $_POST['bio'], time() + 30 * 24 * 60 * 60);
    }
    

    if ($errors) {
    // При наличии ошибок перезагружаем страницу и завершаем работу скрипта.
    header('Location: index.php');
    exit();
    }
    else {
    // Удаляем Cookies с признаками ошибок.
    setcookie('fio_error', '', 100000);
    setcookie('email_error', '', 100000);
    setcookie('power_error', '', 100000);            
    }

    // Сохранение в XML-документ.
    
     $user = 'user1';
     $pass = 'user1';
     $db = new PDO('mysql:host=localhost;dbname=task1', $user, $pass, array(PDO::ATTR_PERSISTENT => true));
     try {
         $stmt = $db->prepare("INSERT INTO users (name, year, sex, email, bio, limb, ab_god, ab_clip, ab_fly) VALUES (:name, :year, :sex, :email, :bio, :limb, :god, :clip, :fly)");
         $stmt->bindParam(':name', $_POST['fio']);
         $stmt->bindParam(':year', $_POST['year']);
         $stmt->bindParam(':sex', $_POST['sex']);
         $stmt->bindParam(':email', $_POST['email']);
         $stmt->bindParam(':bio', $_POST['bio']);
         $stmt->bindParam(':limb', intval($_POST['limb']));
         $stmt->bindParam(':god', intval(in_array("god", $_POST['power'])));
         $stmt->bindParam(':clip', intval(in_array("clip", $_POST['power'])));
         $stmt->bindParam(':fly', intval(in_array("fly", $_POST['power'])));
         $stmt->execute();
     } catch(PDOException $e) {
         print('Error : ' . $e->getMessage());
         exit();
     }
             
    // Сохраняем куку с признаком успешного сохранения.
    setcookie('save', '1');

    // Делаем перенаправление.
    header('Location: index.php');
}
?>
